# XFCE schema

XFCE schemas same as Gnome schemas

95_xfce.gschema.override

The file **95_xfce.gschema.override** is used in the ISO (cnchi Gnome based)

**File locate at**:

```
/usr/share/glib-2.0/schemas/95_xfce.gschema.override
```

To compile schema:

```
/usr/bin/glib-compile-schemas /usr/share/glib-2.0/schemas
```

